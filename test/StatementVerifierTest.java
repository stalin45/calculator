import com.leti.calculator.utility.StatementVerifier;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class StatementVerifierTest {

    @Test
    public void correctStatementTest() {
        String statement = "(ab*d)*d";
        Boolean result = StatementVerifier.isStatementCorrect(statement);
        assertEquals(true, result);
    }

    @Test
    public void incorrectStatementParenthesis() {
        String statement = "(ad))*b";
        Boolean result = StatementVerifier.isStatementCorrect(statement);
        assertEquals(false, result);
    }

    @Test
    public void correctStatementManyParenthesis() {
        String statement = "(((a+1)+1)-1)-(1-(1+(1-1)))";
        Boolean result = StatementVerifier.isStatementCorrect(statement);
        assertEquals(true, result);
    }

    @Test
    public void incorrectStatementDoubleOperand() {
        String statement = "ab**d";
        Boolean result = StatementVerifier.isStatementCorrect(statement);
        assertEquals(false, result);
    }
}
