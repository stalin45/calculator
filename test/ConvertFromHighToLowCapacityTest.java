import com.leti.calculator.Calculator;
import com.leti.calculator.impl.CalculatorImpl;
import org.junit.BeforeClass;
import org.junit.Test;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigInteger;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by Vlastelin on 11.04.2016.
 */
public class ConvertFromHighToLowCapacityTest {

    private static Method convertMethod;

    private static Calculator calculator;

    @BeforeClass
    public static void setUp() throws NoSuchMethodException {
        convertMethod = CalculatorImpl.class.getDeclaredMethod("convertFromHighToLowCapacity", String.class);
        convertMethod.setAccessible(true);

        calculator = new CalculatorImpl();
    }

    @Test
    public void zeroNumberTest() throws InvocationTargetException, IllegalAccessException {
        String statement = "0";

        BigInteger resultInt = (BigInteger) convertMethod.invoke(calculator, statement);
        assertEquals(BigInteger.ZERO, resultInt);
    }

    @Test
    public void lengthMoreThanMax() throws InvocationTargetException, IllegalAccessException {
        String statement = "123456789112345678911234567891123";

        try {
            convertMethod.invoke(calculator, statement);
        } catch(InvocationTargetException ex) {
            assertTrue(ex.getCause() instanceof RuntimeException);
        }
    }

    @Test
    public void lengthLessThanMax() throws InvocationTargetException, IllegalAccessException {
        String statement = "12345678911234567891123456789112";
        String expectedResult = "6660629928049221431456479375700737668737114";

        BigInteger resultInt = (BigInteger) convertMethod.invoke(calculator, statement);
        assertEquals(new BigInteger(expectedResult), resultInt);
    }

    @Test
    public void oneLetterTest() throws InvocationTargetException, IllegalAccessException {
        String statement = "A";
        String expectedResult = "10";

        BigInteger resultInt = (BigInteger) convertMethod.invoke(calculator, statement);
        assertEquals(new BigInteger(expectedResult), resultInt);
    }

    @Test
    public void oneLetterOneFigureTest() throws InvocationTargetException, IllegalAccessException {
        String statement = "3A";
        String expectedResult = "82";

        BigInteger resultInt = (BigInteger) convertMethod.invoke(calculator, statement);
        assertEquals(new BigInteger(expectedResult), resultInt);
    }


    @Test
    public void fractionTest() throws InvocationTargetException, IllegalAccessException {
        String statement = "3.A";

        try {
            convertMethod.invoke(calculator, statement);
        } catch(InvocationTargetException ex) {
            assertTrue(ex.getCause() instanceof RuntimeException);
        }
    }
}
