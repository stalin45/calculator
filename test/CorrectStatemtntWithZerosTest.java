import com.leti.calculator.utility.StatementVerifier;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 * Created by Vlastelin on 18.04.2016.
 */
public class CorrectStatemtntWithZerosTest {

    @Test
    public void minusNumberAfterOpenParenthesisTest() {
        String statement = "ab+(-a)";
        String expectedResult = "ab+(0-a)";
        String actualResult = StatementVerifier.correctStatementWithZeros(statement);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void minusNumberFirstTest() {
        String statement = "-a+ab";
        String expectedResult = "0-a+ab";
        String actualResult = StatementVerifier.correctStatementWithZeros(statement);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void minusAfterNummberTest() {
        String statement = "ab-a";
        String expectedResult = "ab-a";
        String actualResult = StatementVerifier.correctStatementWithZeros(statement);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void stetamentWithoutMinusTest() {
        String statement = "ab+a";
        String expectedResult = "ab+a";
        String actualResult = StatementVerifier.correctStatementWithZeros(statement);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void manyMinusesTest() {
        String statement = "-ab-a*(-dd+(-ad*3)-3)";
        String expectedResult = "0-ab-a*(0-dd+(0-ad*3)-3)";
        String actualResult = StatementVerifier.correctStatementWithZeros(statement);
        assertEquals(expectedResult, actualResult);
    }
}
