import com.leti.calculator.Calculator;
import com.leti.calculator.impl.CalculatorImpl;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by Vlastelin on 05.04.2016.
 */
public class CalculatorEvaluateTest {

    private static Calculator calculator;

    @BeforeClass
    public static void setUp() {
        calculator = new CalculatorImpl();
    }

    @Test(expected = RuntimeException.class)
    public void evaluateIncorrectFigureTest() {
        String statement = "ad*s";
        calculator.evaluate(statement);
    }

    @Test(expected = RuntimeException.class)
    public void evaluateStatementMoreThanMaxSymbols() {
        String statement = "123456789112345678911234567891123";
        calculator.evaluate(statement);
    }

    @Test
    public void evaluateCorrectStatementTest() {
        String statement = "ad*d";
        String result = calculator.evaluate(statement);
        System.out.println(result);
        assertEquals("5H1", result);
    }
}