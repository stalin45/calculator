package com.leti.calculator.impl;

import com.leti.calculator.Calculator;
import com.leti.calculator.utility.StatementVerifier;

import java.math.BigInteger;
import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Scanner;

/**
 * Class for evaluation of arithmetical statements
 */
public class CalculatorImpl implements Calculator
{
    private static final BigInteger CAPACITY = new BigInteger("24");

    private static final int MAX_NUMBER_LENGTH = 32;

    private static String PATTERN_24 = "[a-nA-N]";

    private static final Character[] ARR_CHAR_24 = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd',
                                                    'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n'};

    private static final String ZERO = "0";

    /**
     * Constructor with no arguments
     */
    public CalculatorImpl() {}
     
     /**
      * Check an arithmetical priority related to current operator
      * 
      * @param operator   contains current operator
      * @return           priority of current operator
      */
     private int priority(char operator) {
        switch (operator) {
            case '+':
            case '-':
                return 1;
            case '*':
            case '/':
                return 2;
            default:         //In cases of '(' and ')'
                return -1;
        }
     }
     
      /**
      * Receive and figure out a value of last 2 numbers from <code>stack</code> in order of reverse polish notation 
      * and return it back to <code>stack</code>
      * 
      * @param stack     contains numbers 
      * @param operator  contains last operator from the <code>operators</code> stack 
      * 
      */   
     private void processOperator(ArrayDeque<BigInteger> stack, char operator) {
        BigInteger rightOperand = stack.pollLast();
        BigInteger leftOperand = stack.pollLast();
        switch (operator) {
            case '+':
                stack.add(leftOperand.add(rightOperand));
                break;
            case '-':
                stack.add(leftOperand.subtract(rightOperand));
                break;
            case '*':
                stack.add(leftOperand.multiply(rightOperand));
                break;
            case '/':
                stack.add(leftOperand.divide(rightOperand));
                break;
        }
     }
     
    /**
     * Evaluate statement represented as string in order of reverse polish notation
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     *
     * @return string value containing result of evaluation or exception
     */
    public String evaluate(String statement) {
        if (!StatementVerifier.isStatementCorrect(statement)) {
            throw new RuntimeException("Incorrect statement!");
        }

        statement = StatementVerifier.correctStatementWithZeros(statement);

        ArrayDeque<BigInteger> numbers = new ArrayDeque<>();   //Stack for numbers
        ArrayDeque<Character> operators = new ArrayDeque<>();  //Stack for operators
        for (int i = 0; i < statement.length(); i++) {
            char c = statement.charAt(i);
            switch (c) {
                case ' ':
                    break;
                case '(':
                    operators.add('(');
                    break;
                case ')':
                    while (operators.peekLast() != '(') {
                        processOperator(numbers, operators.pollLast());
                    }
                    operators.pollLast();
                    break;
                case '-':
                case '+':
                case '*':
                case '/':
                    while (!operators.isEmpty() && priority(c) <= priority(operators.peekLast())) {
                        processOperator(numbers, operators.pollLast());
                    }
                    operators.add(c);
                    break;
                default:
                    if (Character.isDigit(c) || Character.toString(c).matches(PATTERN_24)) {
                        String operand = "";
                        while (i < statement.length() &&
                               (Character.isDigit(statement.charAt(i)) ||
                                Character.toString(statement.charAt(i)).matches(PATTERN_24))) {
                            operand += statement.charAt(i++);
                        }
                        numbers.add(convertFromHighToLowCapacity(operand));
                        i--;
                    }
            }
        }
        while (!operators.isEmpty()) {
            processOperator(numbers, operators.pollLast());
        }
        return convertFrom10To24(numbers.pollLast());
    }

    /**
     * Converts figure from 10x to 24x
     *
     * @param number number in 10x
     * @return string with number in 24x
     */
    private static String convertFrom10To24(BigInteger number) {
        if (number.toString().equals(ZERO)) {
            return ZERO;
        }

        BigInteger quotient = new BigInteger(String.valueOf(number)).abs();
        StringBuilder result = new StringBuilder();

        do {
            result.append(ARR_CHAR_24[quotient.mod(CAPACITY).intValue()]);
            quotient = quotient.divide(CAPACITY);
        } while ( ! quotient.toString().equals(ZERO));

        if (number.compareTo(BigInteger.ZERO) < 0) {
            result.append("-");
        }

        return result.reverse().toString().toUpperCase();
    }

    /**
     * Converts figure from 24x to 10x
     *
     * @param number string with number in 24x
     * @return number in 24x
     */
    private static BigInteger convertFromHighToLowCapacity(String number) {
        if (number.length() > MAX_NUMBER_LENGTH) {
            throw new RuntimeException("Number length is more than " + MAX_NUMBER_LENGTH + " !");
        }

        if (number.contains(".")) {
            throw new RuntimeException("Number format is incorrect (Frections are not allowed");
        }

        if (number.equals(ZERO)) {
            return new BigInteger(ZERO);
        }

        char[] reversedCharArr = new StringBuilder(number.toLowerCase()).reverse().toString().toCharArray();
        BigInteger result = new BigInteger(ZERO);

        for (int i = 0; i < reversedCharArr.length; i++) {
            String number10 = String.valueOf( Arrays.asList(ARR_CHAR_24) .indexOf(reversedCharArr[i]) );
                result = result.add(new BigInteger(number10).multiply(CAPACITY.pow(i)));
        }

        return result;
    }
       
    public static void main( String[] args )
    {
        Scanner scanner = new Scanner(System.in);
        CalculatorImpl calc = new CalculatorImpl();
        String result;
        System.out.println("Technology of development\n" +
                           "Comand #10, \n" +
                           "Ibragimova Alina, 1374 \n" +
                           "Kapralov Ilia, 1373 \n" +
                           "Fatkulina Elvira, 1373 \n" +
                           "24-base\n");
        while (true) {
                System.out.print("Please, input a statement: ");

            try {
                result = calc.evaluate(scanner.nextLine());
                System.out.println("Answer: " + result);
            } catch(Exception ex) {
                System.out.println("Error. " + ex.getMessage());
            }

            System.out.print("Do you want to continue (y/n): ");
            String contAns;
            do {
                contAns = scanner.nextLine();
                if (contAns.contains("n")) {
                    System.exit(0);
                }
            } while ( ! contAns.contains("y"));
        }
    }
}
