package com.leti.calculator.utility;

import java.util.ArrayDeque;

public class StatementVerifier {

    /**
     * Constructor with no arguments
     */
    private StatementVerifier() {}

    /**
     * Enumeration for test of correctness of statement
     */
    enum StatementParts {
        PL_DEV_MULT, MINUS,   //operators
        NUMBER,               //numbers
        OPEN_PAR, CLOSE_PAR   //parts
    }

    /**
     * Verify correctness of statement by using 2 stacks:
     * <code>parenthesis</code>  to prove parenthesis validity (contains only parenthesis),
     * <code>parts</code>        to prove correctnes of logic of statement in a whole
     *
     * @param statement contains source string statement
     *
     * @return true if statement is correct, in other case false
     */
    public static boolean isStatementCorrect(String statement)
    {
        ArrayDeque<StatementParts> parts  = new ArrayDeque<StatementParts>();
        ArrayDeque<Character> parenthesis = new ArrayDeque<Character>();
        for (int i=0; i<statement.length(); i++)
        {
            Character c = statement.charAt(i);
            switch(c) {
                case '(':
                    parenthesis.add(c);
                    if (parts.isEmpty() ||    //can be first
                        parts.peekLast() == StatementParts.MINUS ||
                        parts.peekLast() == StatementParts.PL_DEV_MULT ||
                        parts.peekLast() == StatementParts.OPEN_PAR)
                            parts.add(StatementParts.OPEN_PAR);
                    else
                        return false;
                    break;
                case ')':
                    if (parenthesis.isEmpty())
                        return false;
                    parenthesis.pollLast();
                    StatementParts temp = parts.pollLast();    //It must not be like OPEN_PAR - NUMBER - CLOSE_PAR
                    if (parts.size() < 2 ||
                            temp == StatementParts.NUMBER &&
                            parts.peekLast() == StatementParts.OPEN_PAR
                            )
                    {
                        return false;
                    }
                    else {
                        parts.add(temp);                                        //return "temp" in the stack
                        parts.add(StatementParts.CLOSE_PAR);
                    }
                    break;
                case '-':
                    if (parts.isEmpty() ||                                      //can be first
                            parts.peekLast() == StatementParts.OPEN_PAR || parts.peekLast() == StatementParts.CLOSE_PAR ||
                            parts.peekLast() == StatementParts.NUMBER)
                        parts.add(StatementParts.MINUS);
                    else
                        return false;
                    break;
                case '+':
                case '/':
                case '*':
                    if (!parts.isEmpty() &&
                            (parts.peekLast() == StatementParts.NUMBER || parts.peekLast() == StatementParts.CLOSE_PAR))
                        parts.add(StatementParts.PL_DEV_MULT);
                    else
                        return false;
                    break;
                case ' ':
                    continue;
                case '.':
                    if (parts.isEmpty() ||
                            !(parts.peekLast() == StatementParts.NUMBER && i<statement.length() && Character.isDigit(statement.charAt(++i)))  )
                        return false;
                    break;
                default:
                    if (Character.isDigit(c) || Character.toString(c).matches("[a-nA-N]")) {
                        if (parts.isEmpty() ||                                  //can be first
                                parts.peekLast() == StatementParts.NUMBER || parts.peekLast() == StatementParts.OPEN_PAR ||
                                parts.peekLast() == StatementParts.MINUS || parts.peekLast() == StatementParts.PL_DEV_MULT)
                        {
                            if (!(parts.peekLast() == StatementParts.NUMBER))   //To avoid multi-notes
                                parts.add(StatementParts.NUMBER);
                        } else
                            return false;
                    } else
                        return false;
            }
        }
        return parenthesis.isEmpty() && parts.peekLast() != StatementParts.MINUS && parts.peekLast() != StatementParts.PL_DEV_MULT;
    }

    public static String correctStatementWithZeros(String statement) {
        StringBuilder zeroFilledStatement = new StringBuilder();
        for (int i=0; i<statement.length(); i++) {
            Character c = statement.charAt(i);
            if (c.equals('-') &&
                (i == 0 || ( (Character) (statement.charAt(i-1)) ).equals('('))
               ) {
                zeroFilledStatement.append("0").append(c);
            } else {
                zeroFilledStatement.append(c);
            }
        }

        return zeroFilledStatement.toString();
    }
}
